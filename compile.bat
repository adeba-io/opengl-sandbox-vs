@echo off
g++^
 -DGLEW_STATIC^
 .\Source\application.cpp^
 .\Source\_ogl_head.cpp .\Source\GLData\*.cpp .\Source\Scenes\*.cpp .\Source\Visual\*.cpp^
 .\Source\imgui\*.cpp .\Source\stb_image\*.cpp^
 -ISource\ -IDependencies\ -LLibraries\ -lglew32 -lglfw3 -lopengl32 -lglu32 -lgdi32^
 -o OpenGL.exe
