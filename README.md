# OpenGL Sandbox VS

A playground for me to practice implementing OpenGL
Started in Microsoft Visual Studio (see in the `visual-studio` branch), now done using MINGW-G++.

## Project Termination

The purpose of this project was to get to grips with 3D graphics using OpenGL, in preparation for working on my own
video game engine. I think I have gotten to the point in this project where I have learnt enough to move on to the engine.
I would still like to have a 3D graphics sandbox, but if I were to continue I would need a more advanced renderer, a materials system,
an editor, etc. all of which I would make in making the game engine.

When I have gotten all of that done for the game enegine, I will (hopefully remeber to) come back and leave a link to the new 3D Graphics Sandbox here.

## Notes

### Vertex Arrays

Some say its slower to have multiple vertex arrays, rather than having one global one and updating the vertex attribute.
Stress test this is in the deployment environment, and pick the appropriate method.

## VertexBuffers

If the associated vertex buffer is deleted before the object is drawn, an error will be thrown.
With the current API, this means that vertex buffers must be stored alongside vertex arrays and index buffers.

## Matricies

The order of parameters in a operation involving matricies can affect the result. If a matrix operation isn't working as you would have thought try switching the order of operations.

## OpenGL MVP Matrix

Projection - The 'space' we deal with; perspective or orthographic
View - position, rotation, and scale of the 'camera'
Model - position, rotation, and scale of the object being drawn

In OpenGL specifically, the MVP is multiplied (p)rojection * (v)iew * (m)odel
because OpenGL uses column major ordering
glm::mat4 mvp = projection * view * model;

## IMGui Demo

Can be found in `OpenGLSandbox\Source\Vendor\imgui\main.cpp`
