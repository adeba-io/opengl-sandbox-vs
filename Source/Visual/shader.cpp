#include "shader.h"

#include "../_ogl_head.h"
#include <fstream>
#include <sstream>
#include <iostream>

Shader::Shader(const std::string& filepath) : _filepath(filepath)
{
    ShaderProgramSource source = Parse(filepath);
    _id = CreateProgram(source.VertexSource, source.FragmentSource);
}

Shader::~Shader()
{
    GLCall(glDeleteProgram(_id));
}


void Shader::Bind() const
{
    GLCall(glUseProgram(_id));
}

void Shader::SetUniform1i(const char* uniform, int value)
{
    GLCall(glUniform1i(GetUniformLocation(uniform), value));
}

void Shader::SetUniform1f(const char* uniform, float value)
{
    GLCall(glUniform1f(GetUniformLocation(uniform), value));
}

void Shader::SetUniform4f(const char *uniform, float a, float b, float c, float d)
{
    GLCall(glUniform4f(GetUniformLocation(uniform), a, b, c, d));
}

void Shader::SetUniformMat4f(const char* uniform, const glm::mat4& matrix)
{
    GLCall(glUniformMatrix4fv(GetUniformLocation(uniform), 1, GL_FALSE, &matrix[0][0]));
}

void Shader::UnBind()
{
    GLCall(glUseProgram(0));
}

int Shader::GetUniformLocation(const char *uniform)
{
    if (_uniformMap.find(uniform) != _uniformMap.end())
        return _uniformMap[uniform];
    
    GLCall(int uniformID = glGetUniformLocation(_id, uniform));
    _uniformMap[uniform] = uniformID;

    if (uniformID == -1)
        std::cout << "Uniform " << uniform << " does not exist in shader " << _filepath << std::endl;

    return uniformID;
}

ShaderProgramSource Shader::Parse(const std::string& filepath)
{
    std::ifstream stream(filepath);

    enum class ShaderType
    {
        NONE = -1, VERTEX = 0, FRAGMENT = 1
    };

    std::string line;
    std::stringstream ss[2];
    ShaderType type = ShaderType::NONE;

    while (getline(stream, line))
    {
        if (line.find("#shader") != std::string::npos)
        {
            if (line.find("vertex") != std::string::npos)
                type = ShaderType::VERTEX;
            else if (line.find("fragment") != std::string::npos)
                type = ShaderType::FRAGMENT;

            continue;
        }

        if (type == ShaderType::NONE)
            continue;

        ss[(int)type] << line << '\n';
    }

    return { ss[0].str(), ss[1].str() };
}

unsigned int Shader::Compile(unsigned int type, const std::string& source)
{
    GLCall(unsigned int id = glCreateShader(type));
    const char* src = source.c_str();
    GLCall(glShaderSource(id, 1, &src, nullptr));
    GLCall(glCompileShader(id));

    // TODO: Error handling
    int result;
    GLCall(glGetShaderiv(id, GL_COMPILE_STATUS, &result));
    if (result == GL_FALSE)
    {
        int length;
        GLCall(glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length));
        char* message = (char*)_alloca(sizeof(char) * length);
        GLCall(glGetShaderInfoLog(id, length, &length, message));
        std::cout << "Failed to compile " << (type == GL_VERTEX_SHADER ? "vertex" : "fragment") << " shader:" << std::endl;
        std::cout << message << std::endl;
        GLCall(glDeleteShader(id));
        return 0;
    }

    return id;
}

unsigned int Shader::CreateProgram(const std::string& vertexShader, const std::string& fragmentShader)
{
    GLCall(unsigned int program = glCreateProgram());
    GLCall(unsigned int vs = Shader::Compile(GL_VERTEX_SHADER, vertexShader));
    GLCall(unsigned int fs = Shader::Compile(GL_FRAGMENT_SHADER, fragmentShader));

    GLCall(glAttachShader(program, vs));
    GLCall(glAttachShader(program, fs));
    GLCall(glLinkProgram(program));
    GLCall(glValidateProgram(program));

    GLCall(glDeleteShader(vs));
    GLCall(glDeleteShader(fs));
    return program;
}
