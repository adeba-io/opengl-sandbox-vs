#pragma once

#include "Visual/renderer.h"

class Texture
{
private:
	unsigned int _id;
	std::string _filepath;
	unsigned char *_localBuffer;
	
	int _width, _height, _bitsPerPixel;

public:
	Texture(const std::string& filepath);
	~Texture();

	void Bind(unsigned int slot = 0) const;

	inline int GetWidth() const { return _width; }
	inline int GetHeight() const { return _height; }
	
	static void UnBind();
};

