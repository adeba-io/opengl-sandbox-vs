#include "renderer.h"
#include "_ogl_head.h"

#include <iostream>

void Renderer::Clear() const
{
    GLCall(glClear(GL_COLOR_BUFFER_BIT));
}

void Renderer::Draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader) const
{
    shader.Bind();

    va.Bind();
    ib.Bind();
    // No need to bind the vertex buffer as it should be associated with the vertex array

    // glDrawArrays(GL_TRIANGLES, 0, 6); is only used when we're not using an index buffer
    GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));
}


void Renderer::SetClearColour(const glm::vec4 colour) const
{
    GLCall(glClearColor(colour.r, colour.g, colour.b, 1.0f));
}
