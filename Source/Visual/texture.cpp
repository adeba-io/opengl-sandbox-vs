#include "texture.h"

#include "stb_image\stb_image.h"

Texture::Texture(const std::string& filepath) 
	: _id(0), _filepath(filepath), _localBuffer(nullptr), _width(0), _height(0), _bitsPerPixel(0)
{
	stbi_set_flip_vertically_on_load(true);
	_localBuffer = stbi_load(filepath.c_str(), &_width, &_height, &_bitsPerPixel, 4);

	GLCall(glGenTextures(1, &_id));
	Bind();

	// Need to specify these parameters as the texture will not be drawn otherwise

	// How do you want the texture to scale up and scale down
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
	// Should the texture wrap, GL_CLAMP maens no
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));

	GLCall(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, _localBuffer));
	UnBind();

	if (_localBuffer)
		stbi_image_free(_localBuffer);
}

Texture::~Texture()
{
	GLCall(glDeleteTextures(1, &_id));
}

void Texture::Bind(unsigned int slot /* = 0 */) const
{
	// Looking at the defintion for GL_TEXTURE0, it goes up to GL_TEXTURE31
	// Fourtunately, they are consecutive numbers, therefore adding slot is fine
	GLCall(glActiveTexture(GL_TEXTURE0 + slot));
	GLCall(glBindTexture(GL_TEXTURE_2D, _id));
}

void Texture::UnBind()
{
	GLCall(glBindTexture(GL_TEXTURE_2D, 0));
}
