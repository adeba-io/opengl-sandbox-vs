#pragma once

#include <string>
#include <map>
#include "glm/glm.hpp"

struct ShaderProgramSource
{
	std::string VertexSource;
	std::string FragmentSource;
};

class Shader
{
private:
	unsigned int _id;
	const std::string _filepath;
	std::map<std::string, int> _uniformMap;

public:
	Shader(const std::string& filepath);
	~Shader();

	void Bind() const;

	void SetUniform1i(const char* uniform, int value);
	void SetUniform1f(const char* uniform, float value);
	void SetUniform4f(const char *uniform, float a, float b, float c, float d);
	void SetUniformMat4f(const char *uniform, const glm::mat4& matrix);

	static void UnBind();

private:
	int GetUniformLocation(const char* uniform);

public:
	static ShaderProgramSource Parse(const std::string& filepath);
	static unsigned int Compile(unsigned int type, const std::string& source);
	static unsigned int CreateProgram(const std::string& vertexShader, const std::string& fragmentShader);
};

