#pragma once

#include "GLData/vertexArray.h"
#include "GLData/indexBuffer.h"
#include "Visual/shader.h"
#include "glm/glm.hpp"

class Renderer
{
public:
    void Draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader) const;
    void Clear() const;
    void SetClearColour(const glm::vec4 colour) const;
};

