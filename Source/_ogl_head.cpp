#include "_ogl_head.h"
#include <iostream>

void __debugbreak() {}

void GLClearError()
{
    while (glGetError() != GL_NO_ERROR);
    // while (!glGetError()); Could be written like this
}

bool GLLogCall(const char* function, const char* file, int line)
{
    bool success = true;
    while (GLenum error = glGetError())
    {
        std::cout << "[OpenGL Error] (" << error << "): "
            << function << ", " << file << ":" << line << std::endl;
        success = false;
    }
    return success;
}
