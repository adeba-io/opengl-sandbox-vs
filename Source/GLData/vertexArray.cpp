#include "vertexArray.h"
#include "../_ogl_head.h"

VertexArray::VertexArray()
{
    GLCall(glGenVertexArrays(1, &_id));
}

VertexArray::~VertexArray() 
{
    GLCall(glDeleteVertexArrays(1, &_id));
}

void VertexArray::Bind() const
{
    GLCall(glBindVertexArray(_id));
}

void VertexArray::AddBuffer(const VertexBuffer& vb, const VertexBufferLayout& layout)
{
    Bind();
    vb.Bind();
    const auto& elements = layout.GetElements();
    unsigned int offset = 0;

    for (unsigned int i = 0; i < elements.size(); i++)
    {
        const auto& element = elements[i];
        GLCall(glEnableVertexAttribArray(i));
        // Binds the vertex attrib to the vertex array object `vao`
        GLCall(
            glVertexAttribPointer(
                i, element.count, element.type, element.normalized, \
                layout.GetStride(), (const void*)offset)
        );
        offset += element.count * VertexBufferElement::GetSizeOfType(element.type);
    }
}

void VertexArray::UnBind()
{
    GLCall(glBindVertexArray(0));
}
