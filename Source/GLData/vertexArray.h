#pragma once

#include "vertexBuffer.h"
#include "vertexBufferLayout.h"

class VertexArray
{
private:
	unsigned int _id;

public:
	VertexArray();
	~VertexArray();

	void Bind() const;
	void AddBuffer(const VertexBuffer& vb, const VertexBufferLayout& layout);

	static void UnBind();
};

