#pragma once

class IndexBuffer
{
private:
	unsigned int _id;
	unsigned int _count;

public:
	IndexBuffer(const unsigned int *indicies, int count);
	~IndexBuffer();

	void Bind() const;

	inline unsigned int GetCount() const { return _count;  }

	static void UnBind();
};

