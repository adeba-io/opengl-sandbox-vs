#include "_ogl_head.h"

#include <iostream>
#include <string>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"

#include "Visual/renderer.h"

#include "Scenes/_head.h"

const float viewMax = 1000.f;

int main(void)
{
    GLFWwindow *window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    // Sets the OpenGL profile from 'compatible' to 'core'
    // This maese certain operations manual, such as creating vertex arrays
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(960, 540, "Open GL", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    glfwSwapInterval(1);
    
    if (glewInit() != GLEW_OK)
        std::cout << "Error!" << std::endl;

    std::cout << glGetString(GL_VERSION) << std::endl;

    {
        std::vector<scn::Scene*> scenes;
        scenes.push_back(new scn::ClearColourScene());
        scenes.push_back(new scn::SimpleRender());
        scenes.push_back(new scn::TexturedScene());
        scenes.push_back(new scn::MultiScene());
        scenes.push_back(new scn::VertexShaderScene());

        scn::MasterScene master(scenes.data(), scenes.size());

        GLCall(glEnable(GL_BLEND));
        GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

        glm::mat4 projection = glm::ortho(0.0f, 960.0f, 0.0f, 540.0f, 0.0f, 1.0f);

        Renderer renderer;

        IMGUI_CHECKVERSION();
        ImGui::CreateContext();

        ImGui::StyleColorsDark();

        ImGui_ImplGlfw_InitForOpenGL(window, true);
        ImGui_ImplOpenGL3_Init("#version 330");

        bool escape = false;

        /* Loop until the user closes the window */
        while (! (glfwWindowShouldClose(window) || escape))
        {
            // Need to get the delta time
            master.Update(1.f / 60.f);

            /* Render here */
            renderer.Clear();
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();

            master.Draw(projection);

            /* Poll for and process events */
            glfwPollEvents();

            // Start the Dear ImGui frame
            ImGui::NewFrame();

            master.GUI(escape);

            // Rendering
            ImGui::Render();
            int display_w, display_h;
            glfwGetFramebufferSize(window, &display_w, &display_h);
            glViewport(0, 0, display_w, display_h);
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

            /* Swap front and back buffers */
            glfwSwapBuffers(window);
        }

        // Cleanup
        ImGui_ImplOpenGL3_Shutdown();
        ImGui_ImplGlfw_Shutdown();
        ImGui::DestroyContext();
    }

    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}
