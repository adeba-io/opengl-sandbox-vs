#include "threeDScene.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

namespace scn
{
	ThreeDScene::ThreeDScene()
		: _position(160.f, 160.f, 200.f), _viewProjection(1.f),
		_rotation(0.f), _toRotate(0.f),
		_vb(nullptr), _ib(nullptr), _shader("Res/Shaders/basic.shader")
	{
		glm::mat4 proj = glm::perspective(30.f, 1.6f, .1f, 100.f);
		_viewProjection = proj * glm::mat4(1.f);

		// HWD: 637.5, 456, 147

		float verticies[] = {
			// Front face
			-124.f,  173.5f,  40.f, 0.16f, 0.82f, // 0
			 124.f,  173.5f,  40.f, 0.49f, 0.82f, // 1	
			-124.f, -173.5f,  40.f, 0.16f, 0.16f, // 2
			 124.f, -173.5f,  40.f, 0.49f, 0.16f, // 3

			// Left face
			-124.f,  173.5f, -40.f, 0.05f, 0.82f, // 4
			-124.f, -173.5f, -40.f, 0.05f, 0.16f, // 5

			// Right face
			 124.f,  173.5f, -40.f, 0.52f, 0.82f, // 6
			 124.f, -173.5f, -40.f, 0.52f, 0.82f, // 7

			// Back Face
			-124.f,  173.5f, -40.f, 0.91f, 0.82f, // 8
			-124.f, -173.5f, -40.f, 0.91f, 0.16f, // 9
		};

		_vb = new VertexBuffer(verticies, sizeof(verticies));
		VertexBufferLayout layout;
		layout.PushFloat(3); // verticies
		layout.PushFloat(2); // quad coord
		_va.AddBuffer(*_vb, layout);

		unsigned int indicies[] = {
			0, 1, 2,
			0, 2, 3,

			0, 2, 4,
			4, 2, 5,

			1, 6, 7,
			1, 7, 3,

			6, 8, 9,
			6, 9, 7
		};

		_ib = new IndexBuffer(indicies, 3 * 4 * 2);
	}

	ThreeDScene::~ThreeDScene()
	{
		delete _vb;
		delete _ib;
	}

	void ThreeDScene::Update(float delta)
	{
		for (int i = 0; i < 3; i++)
		{
			// I think this is a valid lerp
			float diff = (_toRotate[i] - _rotation[i]) * delta;
			_rotation[i] += diff;
			_toRotate[i] -= diff;
		}
	}

	void ThreeDScene::Draw(glm::mat4 projection)
	{
		const glm::mat4 model = glm::translate(glm::mat4(1.f), _position);
		// glm::mat4 model2 = glm::rotate(glm::mat4(1.f), _rotation);
	}

	void ThreeDScene::GUI(bool& escape)
	{
		
	}
}
