#pragma once

#include <string>
#include "glm//glm.hpp"

namespace scn 
{
	class Scene
	{
	public:
		virtual const char* GetTitle() const = 0;
		virtual const char* GetName() const = 0;
		virtual const char* GetCode() const = 0;

		virtual void Update(float delta) { }
		virtual void GUI(bool &escape) { }
		// Should be const, but we don't have a material system
		// thus, the uniform map for the Shader class will have to be
		// updated at runtime
		virtual void Draw(glm::mat4 projection) { }
	};
}
