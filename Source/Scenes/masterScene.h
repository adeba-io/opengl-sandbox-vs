#pragma once
#include "scene.h"
#include "glm/glm.hpp"

namespace scn
{
	class MasterScene : public Scene
	{
	private:
		Scene* _current;
		Scene** _arrScenes;
		size_t _arrScenesSize;

	public:
		MasterScene(Scene** scenes, size_t size);
		~MasterScene();

		inline virtual const char* GetTitle() const { return "Home Menu"; }
		inline virtual const char* GetName() const { return "master"; }
		inline virtual const char* GetCode() const { return "mm"; }

		virtual void Update(float delta);
		virtual void GUI(bool &escape);
		virtual void Draw(glm::mat4 projection);
	};
}
