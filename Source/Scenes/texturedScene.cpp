#include "texturedScene.h"
#include "glm/gtc/matrix_transform.hpp"
#include "imgui/imgui.h"

namespace scn
{
	TexturedScene::TexturedScene()
		: _view(1.0f), _position(200.f, 300.f, 0.f), _showImage1(true),
		_va(new VertexArray()), _vb(nullptr), _ib(nullptr),
		_texture1("Res/Textures/cpp.png"),
		_texture2("Res/Textures/pbr.jpg"),
		_shader("Res/Shaders/textured.shader")
	{
		float verticies[] = {
			-50.0f, -50.0f, 0.0f, 0.0f, // 0
			 50.0f, -50.0f, 1.0f, 0.0f, // 1
			 50.0f,  50.0f, 1.0f, 1.0f, // 2
			-50.0f,  50.0f, 0.0f, 1.0f  // 3
		};

		_vb = new VertexBuffer(verticies, sizeof(verticies));
		VertexBufferLayout layout;
		layout.PushFloat(2); // Verticies
		layout.PushFloat(2); // Quad Coords
		_va->AddBuffer(*_vb, layout);

		unsigned int indicies[] = {
			0, 1, 2,
			2, 3, 0
		};

		_ib = new IndexBuffer(indicies, 6);
	}

	TexturedScene::~TexturedScene()
	{
		delete _va;
		delete _vb;
		delete _ib;
	}
	
	void TexturedScene::Draw(glm::mat4 projection)
	{
		glm::mat4 model = glm::translate(glm::mat4(1.f), _position);

		if (_showImage1)
		{
			_texture1.Bind();
		}
		else
		{
			_texture2.Bind();
			model = glm::scale(model, glm::vec3(1.8f, 1.08f, 0.f));
		}

		glm::mat4 mvp = projection * _view * model;
		
		_shader.Bind();
		_shader.SetUniformMat4f("u_MVP", mvp);
		_shader.SetUniform1i("u_Texture", 0);

		_renderer.Draw(*_va, *_ib, _shader);
	}

	void TexturedScene::GUI(bool& escape)
	{
		ImGui::Checkbox("Show Texture 1", &_showImage1);
	}
}
