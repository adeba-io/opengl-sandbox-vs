#pragma once
#include "scene.h"
#include "glm/glm.hpp"
#include "../GLData/_head.h"
#include "../Visual/_head.h"

namespace scn
{
	class TexturedScene : public Scene
	{
	private:
		const glm::mat4 _view;
		const glm::vec3 _position;

		bool _showImage1;

		VertexArray* _va;
		VertexBuffer* _vb;
		IndexBuffer* _ib;
		Texture _texture1, _texture2;

		Shader _shader;

		Renderer _renderer;

	public:
		TexturedScene();
		~TexturedScene();

		inline virtual const char* GetTitle() const { return "Simple Textures"; }
		inline virtual const char* GetName() const { return "textured"; }
		inline virtual const char* GetCode() const { return "tx"; }

		virtual void Draw(glm::mat4 projection) override;
		virtual void GUI(bool& escape) override;
	};
}
