#include "vertexShaderScene.h"
#include "glm/gtc/matrix_transform.hpp"
#include "imgui/imgui.h"

namespace scn
{
	VertexShaderScene::VertexShaderScene()
		: _view(1.0f), _position(150.f, 130.f, 0.f),
		_va(), _vb(nullptr), _ib(nullptr), _time(0),
		_shader("Res/Shaders/vertexMagic.shader")
	{
		float verticies[] = {
			  0.0f,   0.0f, 0.5f, 0.5f, // 0
			  0.0f, -40.0f, 0.5f, 0.0f, // 1
			 28.3f, -28.3f, 1.0f, 0.0f, // 2
			 40.0f,   0.0f, 1.0f, 0.5f, // 3
			 28.3f,  28.3f, 1.0f, 1.0f, // 4
			  0.0f,  40.0f, 0.5f, 1.0f, // 5
			-28.3f,  28.3f, 0.0f, 1.0f, // 6
			-40.0f,   0.0f, 0.0f, 0.5f, // 7
			-28.3f, -28.3f, 0.0f, 0.0f  // 8
		};

		_vb = new VertexBuffer(verticies, sizeof(verticies));
		VertexBufferLayout layout;
		layout.PushFloat(2); // Verticies
		layout.PushFloat(2); // Quad Coords
		_va.AddBuffer(*_vb, layout);

		unsigned int indicies[] = {
			0, 1, 2,
			0, 2, 3,
			0, 3, 4,
			0, 4, 5,
			0, 5, 6,
			0, 6, 7,
			0, 7, 8,
			0, 8, 1
		};

		_ib = new IndexBuffer(indicies, 3 * 8);
	}

	VertexShaderScene::~VertexShaderScene()
	{
		delete _vb;
		delete _ib;
	}

	void VertexShaderScene::Update(float delta)
	{
		_time += delta;
	}

	void VertexShaderScene::Draw(glm::mat4 projection)
	{
		glm::mat4 model = glm::translate(glm::mat4(1.f), _position);
		glm::mat4 mvp = projection * _view * model;

		_shader.Bind();
		_shader.SetUniformMat4f("u_MVP", mvp);
		_shader.SetUniform1f("u_time", _time);

		_renderer.Draw(_va, *_ib, _shader);
	}

	void VertexShaderScene::GUI(bool& escape)
	{

	}
}
