#pragma once
#include "scene.h"
#include "glm/vec3.hpp"
#include "imgui/imgui.h"
#include "Visual/renderer.h"

namespace scn
{
	class ClearColourScene : public Scene
	{
	private:
		glm::vec4 _clearColour;
		Renderer _renderer;

	public:
		ClearColourScene();

		inline virtual const char* GetTitle() const { return "Clear Colour"; };
		inline virtual const char* GetName() const { return "clearcol"; }
		inline virtual const char* GetCode() const { return "cc"; }

		virtual void GUI(bool& escape);
	};
}
