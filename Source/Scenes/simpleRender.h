#pragma once
#include "scene.h"
#include "glm/glm.hpp"
#include "../GLData/_head.h"
#include "../Visual/_head.h"
#include "Visual/renderer.h"


namespace scn
{
	class SimpleRender : public Scene
	{
	private:
		const glm::mat4 _view;
		glm::vec3 _position;

		Shader _shader;
		VertexArray _va;
		VertexBuffer *_vb;
		IndexBuffer *_ib;

		Renderer _renderer;

	public:
		SimpleRender();
		~SimpleRender();

		inline virtual const char* GetTitle() const { return "Simple Render"; }
		inline virtual const char* GetName() const { return "simplerender"; }
		inline virtual const char* GetCode() const { return "sr"; }

		virtual void Draw(glm::mat4 projection);
		virtual void GUI(bool &escape);
	};
}
