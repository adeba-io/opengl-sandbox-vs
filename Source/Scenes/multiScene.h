#pragma once
#include "scene.h"
#include "glm/glm.hpp"
#include "../GLData/_head.h"
#include "../Visual/_head.h"

namespace scn
{
	class MultiScene : public Scene
	{
	private:
		const glm::mat4 _view;
		const glm::vec3 _position1, _position2;

		VertexArray* _va1, * _va2;
		VertexBuffer* _vb1, * _vb2;
		IndexBuffer* _ib;
		Texture _texture;

		Shader _shader1, _shader2;

		Renderer _renderer;

	public:
		MultiScene();
		~MultiScene();

		inline virtual const char* GetTitle() const { return "Multiple Objects"; }
		inline virtual const char* GetName() const { return "multiscene"; }
		inline virtual const char* GetCode() const { return "ms"; }

		virtual void Draw(glm::mat4 projection);
		virtual void GUI(bool& escape);
	};
}
