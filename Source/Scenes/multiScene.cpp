#include "multiScene.h"
#include "glm/gtc/matrix_transform.hpp"
#include "imgui/imgui.h"

namespace scn
{
	MultiScene::MultiScene()
		: _view(1.0f), _position1(180.f, 180.f, 0.f), _position2(330.f, 450.f, 0.f),
		_va1(new VertexArray()), _va2(new VertexArray()),
		_vb1(nullptr), _vb2(nullptr),
		_ib(nullptr),
		_texture("Res/Textures/cpp.png"),
		_shader1("Res/Shaders/textured.shader"),
		_shader2("Res/Shaders/basic.shader")
	{
		float verticies[] = {
			-50.0f, -50.0f, 0.0f, 0.0f, // 0
			 50.0f, -50.0f, 1.0f, 0.0f, // 1
			 50.0f,  50.0f, 1.0f, 1.0f, // 2
			-50.0f,  50.0f, 0.0f, 1.0f  // 3
		};

		{ 
			// Obj 1
			_vb1 = new VertexBuffer(verticies, sizeof(verticies));
			VertexBufferLayout layout;
			layout.PushFloat(2); // Verticies
			layout.PushFloat(2); // Quad Coords
			_va1->AddBuffer(*_vb1, layout);

			_texture.Bind();
			_shader1.Bind();
			_shader1.SetUniform1i("u_Texture", 0);
		}

		{
			// Obj 2
			_vb2 = new VertexBuffer(verticies, sizeof(verticies));
			VertexBufferLayout layout;
			layout.PushFloat(2); // Verticies
			layout.PushFloat(2); // Quad Coords
			_va2->AddBuffer(*_vb2, layout);
		}

		unsigned int indicies[] = {
			0, 1, 2,
			2, 3, 0
		};

		// Only need onw as both objects are quads
		_ib = new IndexBuffer(indicies, 6);
	}
	
	MultiScene::~MultiScene()
	{
		delete _va1;
		delete _va2;
		delete _vb1;
		delete _vb2;
		delete _ib;
	}

	void MultiScene::Draw(glm::mat4 projection)
	{
		glm::mat4 vp = projection * _view;

		{
			glm::mat4 mvp = vp * glm::translate(glm::mat4(1.f), _position1);
			_shader1.Bind();
			_shader1.SetUniformMat4f("u_MVP", mvp);
			_renderer.Draw(*_va1, *_ib, _shader1);
		}

		{
			glm::mat4 mvp = vp * glm::translate(glm::mat4(1.f), _position2);
			_shader2.Bind();
			_shader2.SetUniformMat4f("u_MVP", mvp);
			_renderer.Draw(*_va2, *_ib, _shader2);
		}
	}

	void MultiScene::GUI(bool& escape)
	{

	}
}
