#include "clearColourScene.h"

namespace scn
{
	ClearColourScene::ClearColourScene() : _clearColour(0, 0, 0, 0) { }

	void ClearColourScene::GUI(bool& escape)
	{
		ImGui::ColorEdit3("Clear Colour", &_clearColour.r);
		_renderer.SetClearColour(_clearColour);
	}
}
