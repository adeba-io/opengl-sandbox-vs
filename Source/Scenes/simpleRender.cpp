#include "simpleRender.h"
#include "glm/gtc/matrix_transform.hpp"
#include "imgui/imgui.h"

namespace scn
{
	SimpleRender::SimpleRender()
		: _view(1.0f),
        _position(0, 0, 0),
		_shader("Res/Shaders/basic.shader"),
		_va(), _vb(nullptr), _ib(nullptr), _renderer()
	{
        float verticies[] = {
            -50.0f, -50.0f, 0.0f, 0.0f, // 0
             50.0f, -50.0f, 1.0f, 0.0f, // 1
             50.0f,  50.0f, 1.0f, 1.0f, // 2
            -50.0f,  50.0f, 0.0f, 1.0f  // 3
        };

        _vb = new VertexBuffer(verticies, 4 * 4 * sizeof(float));
        VertexBufferLayout layout;
        layout.PushFloat(2); // Verticies
        layout.PushFloat(2); // Quad Coords
        _va.AddBuffer(*_vb, layout);

        unsigned int indicies[] = {
            0, 1, 2,
            2, 3, 0
        };
        
        _ib = new IndexBuffer(indicies, 6);
	}

    SimpleRender::~SimpleRender()
    {
        delete _vb;
        delete _ib;
    }

    void SimpleRender::Draw(glm::mat4 projection)
    {
        glm::mat4 model = glm::translate(glm::mat4(1.f), _position);
        // Needs to be multiplied backwards, because OpenGL (and therefore glm)
        // use and assume column major ordering for matrices
        glm::mat4 mvp = projection * _view * model;

        _shader.Bind();
        _shader.SetUniformMat4f("u_MVP", mvp);

        _renderer.Draw(_va, *_ib, _shader);
    }

    void SimpleRender::GUI(bool& escape)
    {
        ImGui::SliderFloat2("Position", &_position.x, -600.f, 600.f);
    }
}
