#pragma once

#include "scene.h"
#include "masterScene.h"
#include "clearColourScene.h"
#include "simpleRender.h"
#include "texturedScene.h"
#include "multiScene.h"
#include "vertexShaderScene.h"
#include "threeDScene.h"
