#pragma once
#include "scene.h"
#include "glm/glm.hpp"
#include "../GLData/_head.h"
#include "../Visual/_head.h"

namespace scn
{
	class ThreeDScene : public Scene
	{
	private:
		const glm::vec3 _position;

		glm::mat4 _viewProjection;

		glm::vec3 _rotation;
		glm::vec3 _toRotate;

		VertexArray _va;
		VertexBuffer* _vb;
		IndexBuffer* _ib;

		Shader _shader;
		Renderer _renderer;

	public:
		ThreeDScene();
		~ThreeDScene();

		inline virtual const char* GetTitle() const { return "3D Scene"; }
		inline virtual const char* GetName() const { return "3d"; }
		inline virtual const char* GetCode() const { return "3d"; }

		virtual void Update(float delta) override;
		virtual void Draw(glm::mat4 projection) override;
		virtual void GUI(bool& escape) override;
	};
}
