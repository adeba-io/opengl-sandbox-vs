#include "masterScene.h"
#include "imgui/imgui.h"
#include <iostream>

namespace scn
{
	MasterScene::MasterScene(Scene** scenes, size_t size)
		: _current(nullptr), _arrScenes(scenes), _arrScenesSize(size)
	{}

	MasterScene::~MasterScene()
	{
		delete _current;
	}

	void MasterScene::Update(float delta)
	{
		if (_current != nullptr)
			_current->Update(delta);
	}

	void MasterScene::GUI(bool& escape)
	{
		ImGui::Begin(
			_current == nullptr ? GetTitle() : _current->GetTitle(), 
			&escape);

		if (_current == nullptr)
		{
			for (size_t i = 0; i < _arrScenesSize; i++)
			{
				Scene* scene = _arrScenes[i];

				if (ImGui::Button(scene->GetName()))
				{
					_current = scene;
					break;
				}
			}
		}
		else
		{
			bool back = ImGui::Button("< Back");

			bool escape = false;
			_current->GUI(escape);

			if (back)
				_current = nullptr;
		}

		ImGui::End();
	}

	void MasterScene::Draw(glm::mat4 projection)
	{
		if (_current != nullptr)
			_current->Draw(projection);
	}
}
