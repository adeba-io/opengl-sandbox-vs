#shader vertex
#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 quadCoord;

out vec2 coord;

uniform mat4 u_MVP;

void main()
{
	gl_Position = u_MVP * position;
	coord = quadCoord;
};

#shader fragment
#version 330 core

layout(location = 0) out vec4 colour;

in vec2 coord;

void main()
{
	colour = vec4(coord.xy, 0.5, 1.0);
};
