#shader vertex
#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 quadCoord;

out vec2 v_QCoord;

uniform mat4 u_MVP;
uniform float u_time;

void main()
{
	vec4 v = position;
	v.x += sin(u_time) * v.x * 0.1f;
	v.y += cos(u_time) * v.y * 0.1f;

	gl_Position = u_MVP * v;
	v_QCoord = quadCoord;
};

#shader fragment
#version 330 core

layout(location = 0) out vec4 colour;

in vec2 v_QCoord;

void main()
{
	colour = vec4(v_QCoord.xy, 0.5f, 1.0f);
};
